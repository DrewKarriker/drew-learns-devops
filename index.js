const express = require('express');
const app = express();
const portNum = 3000
const message='Hello World'

app.get('/', function(req, res){
    res.send(message);
    }
);

let server = app.listen(portNum, function(){
    let port = server.address().port;
    console.log('Demo app is now listening on http://%s:%d', port);
});